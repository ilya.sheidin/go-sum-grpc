FROM golang:latest
RUN apt-get update && \
    apt-get -y install git unzip build-essential autoconf libtool apt-transport-https
RUN git clone https://github.com/google/protobuf.git && \
    cd protobuf && \
    ./autogen.sh && \
    ./configure && \
    make && \
    make install && \
    ldconfig && \
    make clean && \
    cd .. && \
    rm -r protobuf
WORKDIR /app
RUN go get -u github.com/golang/protobuf/protoc-gen-go
COPY ./calculator/calculator_server /app
RUN go mod tidy
RUN go mod vendor

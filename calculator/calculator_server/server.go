package main

import (
	"context"
	"fmt"
	calculatorpb "gitlab.com/ilya.sheidin/go-sum-grpc/calculator/calculator_server/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"math"
	"net"
	"time"
)

type CalculatorServiceServer struct {
}

func (*CalculatorServiceServer) SquareRoot(ctx context.Context, req *calculatorpb.SquareRootRequest) (*calculatorpb.SquareRootResponse, error) {
	number := req.GetNumber()
	if number > 0 {
		return &calculatorpb.SquareRootResponse{
			Result: math.Sqrt(float64(number)),
		}, nil
	} else {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Received negative number %v", number))
	}
}

func (*CalculatorServiceServer) FindMaximum(stream calculatorpb.CalculatorService_FindMaximumServer) error {
	maxNumber := int32(0)

	for {
		req, err := stream.Recv()
		fmt.Println("pre Received: ", req.GetNumber())
		if err == io.EOF {
			return nil
		}

		if err != nil {
			log.Fatalf("Failed to receive request: %v", err)
		}
		if req.GetNumber() > maxNumber {
			maxNumber = req.GetNumber()
			err := stream.Send(&calculatorpb.FindMaximumResponse{
				Result: maxNumber,
			})
			if err != nil {
				log.Fatalf("Failed to send response: %v", err)
				return err
			}
		} else {

			fmt.Println("Received: ", req.GetNumber(), " but will be ignored")
		}
	}
}

func (*CalculatorServiceServer) ComputeAverage(stream calculatorpb.CalculatorService_ComputeAverageServer) error {
	fmt.Println("ComputeAverage method invoked")
	var numbers []int32
	total := int32(0)
	for {
		req, err := stream.Recv()

		if err == io.EOF {
			for _, number := range numbers {
				fmt.Println("summing numbers: ", number)
				total = total + number
			}
			result := total / int32(len(numbers))
			fmt.Println(result)

			return stream.SendAndClose(&calculatorpb.ComputeAverageResponse{
				Result: result,
			})
		}

		if err != nil {
			log.Fatalf("Failed to receive stream message: %v", err)
		}
		fmt.Println("Adding number: ", req.GetNumber())
		numbers = append(numbers, req.GetNumber())

	}
}

func (s *CalculatorServiceServer) PrimeNumberDecomposition(req *calculatorpb.PrimeNumberDecompositionRequest, stream calculatorpb.CalculatorService_PrimeNumberDecompositionServer) error {
	divider := int32(2)
	number := req.GetNumber()
	for number > 1 {
		if (number % divider) == 0 {
			err := stream.Send(&calculatorpb.PrimeNumberDecompositionResponse{
				Result: divider,
			})
			if err != nil {
				panic(err)
				return err
			}
			number = number / divider
		} else {
			divider = divider + 1
		}
	}
	return nil
}

func (s *CalculatorServiceServer) SumManyTimes(req *calculatorpb.SumManyTimesRequest, stream calculatorpb.CalculatorService_SumManyTimesServer) error {
	fmt.Println("Sum Many Times Method used")
	for i := 0; i < 10; i++ {
		res := &calculatorpb.SumManyTimesResponse{
			Result: fmt.Sprintf("The %d result is: %v", i, req.GetX()+req.GetY()),
		}
		time.Sleep(2 * time.Second)
		err := stream.Send(res)
		if err != nil {
			panic(err)
			return err
		}

	}

	return nil
}

func (*CalculatorServiceServer) SumNumbers(ctx context.Context, req *calculatorpb.SumCalculatorRequest) (*calculatorpb.SumCalculatorResponse, error) {
	fmt.Println("Sum numbers Method used")
	/*simulate long task*/
	for i := 0; i < 3; i++ {
		if ctx.Err() == context.Canceled {
			return nil, status.Error(codes.Canceled, "The client canceled the request")
		}
		time.Sleep(time.Second)
	}
	return &calculatorpb.SumCalculatorResponse{
		Result: req.GetX() + req.GetY(),
	}, nil
}

func main() {
	fmt.Println("Ready for connection")
	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to create server %v", err)
	}
	s := grpc.NewServer()
	calculatorpb.RegisterCalculatorServiceServer(s, &CalculatorServiceServer{})
	/*Expose GRPC*/
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to listen %v", err)
	}

}

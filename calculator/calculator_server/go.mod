module gitlab.com/ilya.sheidin/go-sum-grpc/calculator/calculator_server

go 1.14

require (
	github.com/golang/protobuf v1.3.3
	google.golang.org/grpc v1.28.0
)

module gitlab.com/ilya.sheidin/go-sum-grpc/calculator/calculator_client

go 1.14

require (
	gitlab.com/ilya.sheidin/go-sum-grpc/calculator/calculator_server v0.0.0-20200322115252-9ea150a57c4d
	google.golang.org/grpc v1.28.0
)

replace gitlab.com/ilya.sheidin/go-sum-grpc/calculator/calculator_server => ../calculator_server

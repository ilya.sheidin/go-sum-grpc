package main

import (
	"context"
	"fmt"
	calculatorpb "gitlab.com/ilya.sheidin/go-sum-grpc/calculator/calculator_server/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"time"
)

func main() {
	sc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to connect to server %v", err)
	}
	c := calculatorpb.NewCalculatorServiceClient(sc)
	//doUnary(c)
	//doServerStreaming(c)
	//primeNumberDecomposition(c, 132)
	//ComputeAverage(c, []int32{10, 70, 31, 20, 75, 99})
	//FindMaximum(c, []int32{1, 5, 3, 6, 2, 20})
	//DoErrorSquareRoot(c, -10)
	doUnaryWithDeadLine(c, 5*time.Second)
	doUnaryWithDeadLine(c, 1*time.Second)
}

func doUnaryWithDeadLine(c calculatorpb.CalculatorServiceClient, dl time.Duration) {
	cont, cancel := context.WithTimeout(context.Background(), dl)
	defer cancel()
	res, err := c.SumNumbers(cont, &calculatorpb.SumCalculatorRequest{
		X: 10,
		Y: 20,
	})
	if err != nil {
		statusErr, grpcError := status.FromError(err)
		if grpcError {

			if statusErr.Code() == codes.DeadlineExceeded {
				log.Fatal("Timeout was reached")
			} else {
				log.Fatalf(" :%v", statusErr.Message())
			}
		} else {
			log.Fatalf("Failed to: %v", statusErr)
		}
	}
	fmt.Println("The result is:", res.GetResult())
}

func DoErrorSquareRoot(c calculatorpb.CalculatorServiceClient, number int32) {
	res, err := c.SquareRoot(context.Background(), &calculatorpb.SquareRootRequest{
		Number: number,
	})
	if err != nil {
		respErr, grpcError := status.FromError(err)
		if grpcError {
			fmt.Println(respErr.Message())
			fmt.Println(respErr.Code())
			if respErr.Code() == codes.InvalidArgument {
				fmt.Println("Negative number sent")
			}
		} else {
			fmt.Println("Something went wrong: ", respErr)
		}
	} else {
		fmt.Println("The number is: ", res.GetResult())
	}

}

func doUnary(c calculatorpb.CalculatorServiceClient) {
	res, err := c.SumNumbers(context.Background(), &calculatorpb.SumCalculatorRequest{
		X: 3,
		Y: 10,
	})
	if err != nil {
		log.Fatalf("Failed with message: %v", err)
	}

	if res != nil {
		fmt.Println("The result is:", res.Result)
	}
}

func doServerStreaming(c calculatorpb.CalculatorServiceClient) {
	stream, err := c.SumManyTimes(context.Background(), &calculatorpb.SumManyTimesRequest{
		X: 500,
		Y: 700,
	})
	if err != nil {
		panic(err)
	}

	for {
		msg, err := stream.Recv()

		if err == io.EOF {
			// we've reached the end of the stream
			break
		}
		if err != nil {
			panic(err)
		}
		fmt.Println(msg.GetResult())
	}

}

func primeNumberDecomposition(c calculatorpb.CalculatorServiceClient, n int32) {
	stream, err := c.PrimeNumberDecomposition(context.Background(), &calculatorpb.PrimeNumberDecompositionRequest{
		Number: n,
	})
	if err != nil {
		panic(err)
	}
	for {
		message, err := stream.Recv()

		if err != nil {
			if err == io.EOF {
				log.Println("Jobs finished")
				break
			} else {
				panic(err)
			}

		}
		fmt.Println(message.GetResult())
	}
}

func ComputeAverage(c calculatorpb.CalculatorServiceClient, numbers []int32) {
	stream, err := c.ComputeAverage(context.Background())
	if err != nil {
		log.Fatalf("Failed to connect ot server: %v", err)
	}
	var listOfMessages []*calculatorpb.ComputeAverageRequest
	for _, number := range numbers {
		listOfMessages = append(listOfMessages, &calculatorpb.ComputeAverageRequest{Number: number})
	}

	for _, message := range listOfMessages {
		err := stream.Send(message)
		if err != nil {
			log.Fatalf("Failed to send message: %v", err)
		}
		time.Sleep(1 * time.Second)
	}
	res, err := stream.CloseAndRecv()

	if err != nil {
		log.Fatalf("Failed to receive response: %v", err)
	}

	fmt.Printf("The avarage is: %v", res.GetResult())

}

func FindMaximum(c calculatorpb.CalculatorServiceClient, numbers []int32) {
	stream, err := c.FindMaximum(context.Background())
	if err != nil {
		log.Fatalf("Failed to open stream: %v", err)
	}
	chanel := make(chan struct{})
	/*send list of numbers*/
	go func() {
		for _, number := range numbers {
			err = stream.Send(&calculatorpb.FindMaximumReqeust{
				Number: number,
			})
			if err != nil {
				log.Fatalf("Failed to send request: %v", err)
			}
		}
		err := stream.CloseSend()
		if err != nil {
			log.Fatalf("Failed to close stream: %v", err)
		}
	}()
	/*receive the highest number*/
	go func() {
		for {
			resp, err := stream.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("Failed to receive response: %v", err)
			}
			log.Printf("Curently the max number is: %v", resp.GetResult())

		}
		close(chanel)

	}()

	<-chanel
	err = stream.CloseSend()
	if err != nil {
		log.Fatalf("Error to close server: %v", err)
	}
}

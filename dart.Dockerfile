FROM google/dart as dart-proto-file
RUN apt-get update && \
    apt-get -y install git unzip build-essential autoconf libtool apt-transport-https
RUN git clone https://github.com/google/protobuf.git && \
    cd protobuf && \
    ./autogen.sh && \
    ./configure && \
    make && \
    make install && \
    ldconfig && \
    make clean && \
    cd .. && \
    rm -r protobuf
WORKDIR /app
ADD ./calculator/calculator_server/proto/calculator.proto /app
RUN pub global activate protoc_plugin
ADD ./generate.sh /generate.sh